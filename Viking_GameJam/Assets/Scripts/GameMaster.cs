﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class GameMaster : MonoBehaviour {

    public static GameMaster current;

    public GameObject player;

    public GameObject[] spawners;

    public float timeWest;


    public GameObject deatpanel;


    private float timeOut;

    private float timeEast;

    private Vector3 starPosition;

    //============================================
    void Start() {



        GameMaster.current = this;

        this.starPosition = GameObject.FindGameObjectWithTag("Player").transform.position;


        //esto es el tiempo de los spawner activados

        this.spawners[1].SetActive(false);

        

        this.timeWest = 0;

        this.timeEast = 0;

        this.timeOut = 5;



    }

    //============================================
    void Update()
    {
        //East

        if (!this.spawners[0].activeSelf)
        {
            this.timeEast += Time.deltaTime;

            if (this.timeEast > this.timeOut)
            {

                this.timeEast = 0;
                this.spawners[0].SetActive(true);
            }

        }
        //West

        if (! this.spawners[1].activeSelf){
            this.timeWest += Time.deltaTime;

            if(this.timeWest > this.timeOut)
            {

                this.timeWest = 0;
                this.spawners[1].SetActive(true);
            }

        }

    }

    //============================================

    public void GameOver()
    {

        this.deatpanel.SetActive(true);
    }

    public void ReloadScene()
    {

        Application.LoadLevel(0);
    }

}



