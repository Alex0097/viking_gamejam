﻿using UnityEngine;
using System.Collections;

public class abuelaInput : MonoBehaviour
{

    //=========================================================
    public float abuelaSpeed;
    public float abuelaJumpimpulso;
    public Transform grundCheckPoint;
    public LayerMask whatisGround;
    public bool ImInGround;


    private Rigidbody2D abuelabody;

    private Vector2 abuelaMovement;

    private float horImput;

    private bool abuelaJum;

    private Animator anim;

    private bool abuelaMirandoParaDondeEs;

    //=======================================================
    void Start()
    {
        this.abuelabody = this.GetComponent<Rigidbody2D>();
        this.abuelaMovement = new Vector2();
        this.ImInGround = false;

        abuelaJumpimpulso = 20;

        this.anim = GetComponent<Animator>();

        this.abuelaMirandoParaDondeEs = true;
    }
    //====================================================

    void Update()
    {

        this.horImput = Input.GetAxis("Horizontal");
        this.abuelaJum = Input.GetKey(KeyCode.Space);


        if((this.horImput <0) && (this.abuelaMirandoParaDondeEs)) {
            this.Flip();
            this.abuelaMirandoParaDondeEs = false;

        } else if((this.horImput > 0) && (!this.abuelaMirandoParaDondeEs))
        {
            this.Flip();
            this.abuelaMirandoParaDondeEs = true;

        }



      //  this.anim.SetFloat("VerSpeed", Mathf.Abs(this.abuelabody.velocity.y));

        if (Physics2D.OverlapCircle(this.grundCheckPoint.position, 0.10f, this.whatisGround))
        {

            this.ImInGround = true;

        }else
        {
            this.ImInGround = false;

        }

    }
    //========================================================
    void FixedUpdate()
    {
        this.abuelaMovement = this.abuelabody.velocity;

        this.abuelaMovement.x = horImput * abuelaSpeed;
        /*
        if (this.abuelaJum && this.ImInGround)
        {
            //  this.abuelaMovement.y = abuelaJumpimpulso;
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0,abuelaJumpimpulso));
        }
        */
        if (Input.GetKeyDown(KeyCode.Space)&& ImInGround)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 200));
        }

        this.abuelabody.velocity = this.abuelaMovement;


    }
    //========================================================
    void Flip()
    {
        Vector3 scale = this.transform.localScale;
        scale.x *= (-1);
        this.transform.localScale = scale;

    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(grundCheckPoint.position,0.01f);
    }
    //========================================================
    public void OnGetKill()
    {
        GameMaster.current.GameOver();

    }

    //========================================================

   
}
