﻿using UnityEngine;
using System.Collections;

public class Player_Info : MonoBehaviour {
    [System.Serializable]
    public class Player_Stats
    {
        public int Salud = 100;
    }

    public Player_Stats Stats = new Player_Stats();

    public void Damage(int damage)
    {

        Stats.Salud -= damage;
       if( Stats.Salud <= 0)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {

	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
