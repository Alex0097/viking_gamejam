﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Cambio_Scena : MonoBehaviour {

    public GameObject Panel_options;
    public GameObject Panel_Credits;

    IEnumerator Change(string scena)
    {
        float fadeTime = GameObject.Find("GM_").GetComponent<Fade>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        SceneManager.LoadScene(scena);
        
    }

    public void LEV_1()
    {
        StartCoroutine(Change("Area_01"));
       
    }
    public void Go_Menu()
    {
        Time.timeScale = 1;
        StartCoroutine(Change("Menu"));
    }
    public void Menu_Options()
    {
        Panel_options.SetActive(true);
    }
    public void Return_options()
    {
        Panel_options.SetActive(false);
        Panel_Credits.SetActive(false);
    }
    public void P_Credits()
    {
        Panel_Credits.SetActive(true);
    }

    public void Exit_app()
    {
        Application.Quit();
    }
    // Use this for initialization
    void Start()
    {
        Panel_options.SetActive(false);
        Panel_Credits.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

}
