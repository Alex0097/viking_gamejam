﻿using UnityEngine;
using System.Collections;

public class Rotation_Light : MonoBehaviour {

    float angle;


    float PosX ;
    float PosY;
    // Use this for initialization
    void Start ()
    {

        PosX = 0;
        PosY = 0;

    }


    public void Joystick_Pod(out float Joystick_X, out float Joystick_Y)
    {
        Joystick_X = Input.GetAxis("Horizontal");
        Joystick_Y = Input.GetAxis("Vertical");

    }

    // Update is called once per frame
    void Update ()
    {



        Joystick_Pod(out PosX, out PosY);

        angle = Mathf.Atan2(PosX, PosY) * Mathf.Rad2Deg;

        if (PosX != 0f && PosY != 0f)
        {
           transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle * -1 -90));
          
        }

    


    }
}
