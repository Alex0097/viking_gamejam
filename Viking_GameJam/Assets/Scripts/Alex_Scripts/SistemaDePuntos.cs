﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SistemaDePuntos : MonoBehaviour
{



    public float puntos_por_Tiempo;
    public int Puntos_porMenos_rebotes;
    public int countRebotes;
    public int Puntos_Por_Collecionables;
    float Minutos;
    float Segundos;
    float  Tiempo;

    public Text[] Puntos_Alfinal;

    float Texto_alpha;

    public GameObject Collecionable;
    GameObject canvasObject;
  //  public RedLine Final_Nivel;
    public int puntosTotales;
    public Text Texto_Puntuaje;
    public Text Texto_Tiempo;
    ParticleSystem Particulas;
    public AudioClip Collected;
    AudioSource Collec_Sound;

    Color c;

    // Use this for initialization
    void Start()
    {
        // canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
        puntos_por_Tiempo = 1200;
        Puntos_porMenos_rebotes = 1000;
        Texto_Tiempo.text = Time.deltaTime.ToString();
        countRebotes = 0;
        Texto_alpha = 0;


        c = new Color(255, 255, 255, 0);
        //  Particulas.Stop();

    }


    void Presentar_Puntuacion(float Transparencia)
    {

        Puntos_Alfinal[0].color = new Color(255, 255, 255, Transparencia);
        Puntos_Alfinal[1].color = new Color(255, 255, 255, Transparencia);
        Puntos_Alfinal[2].color = new Color(255, 255, 255, Transparencia);
        Puntos_Alfinal[3].color = new Color(255, 255, 255, Transparencia);


        Puntos_Alfinal[0].text = "Score: " + Puntos_Por_Collecionables;
        Puntos_Alfinal[1].text = "Time: " + Minutos + ":" + Segundos;
        Puntos_Alfinal[2].text = "Rebounds: " + countRebotes;
        Puntos_Alfinal[3].text = "Total Score: " +  ((int)Puntos_Por_Collecionables +(int) Puntos_porMenos_rebotes + (int)puntos_por_Tiempo).ToString();

    } 
    IEnumerator Particulas_Dead()
    {
     
        Collecionable.GetComponent<Collider2D>().enabled = false;
        Collecionable.GetComponent<Renderer>().enabled = false; 
        Particulas.Play();
      
        yield return new WaitForSeconds(2);
      
        Destroy(Collecionable);


    }
    IEnumerator Coll()
    {
        yield return new WaitForSeconds(0.1f);
        Collecionable = null;
        yield return new WaitForSeconds(0.1f);
    }


    void OnCollisionEnter2D(Collision2D col)
    {


        if (col.gameObject.tag == "Pared")
        {
            Puntos_porMenos_rebotes -= 20;
            countRebotes++;

        }
        if(col.gameObject.tag == "Collect")
        {
            Collecionable = col.gameObject;
            Collec_Sound = Collecionable.GetComponent<AudioSource>();
            Particulas = Collecionable.GetComponent<ParticleSystem>();
            Collec_Sound.PlayOneShot(Collected);
            Puntos_Por_Collecionables += 300;
            StartCoroutine(Particulas_Dead());
           // StartCoroutine(Coll());
            

        }
    }

    // Update is called once per frame
    void Update()
    {
        puntos_por_Tiempo -=40* Time.deltaTime;
        Tiempo += Time.deltaTime;
        Minutos = Mathf.Floor(Tiempo / 60);
        Segundos = (Tiempo % 60);
        Texto_Puntuaje.text = "Score:" + (Puntos_Por_Collecionables).ToString();
        Texto_Tiempo.text = Minutos + ":" + Segundos;
        Presentar_Puntuacion(Texto_alpha);
        /*
        if (Final_Nivel.Presentar_Texto == true)
        {
            Texto_alpha = 255;
        }
        */
        
        
    }
}

