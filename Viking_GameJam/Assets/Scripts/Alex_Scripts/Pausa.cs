﻿using UnityEngine;
using System.Collections;

public class Pausa : MonoBehaviour {

    public GameObject Options_GBJ;


    public void Poner_Pausa()
    {
        GetComponent<Canvas>().enabled = true;
        Time.timeScale = 0;
    }
    public void Quitar_Pausa()
    {

        GetComponent<Canvas>().enabled = false;
        Time.timeScale = 1;
    }
    public void Options()
    {
        Options_GBJ.GetComponent<Canvas>().enabled = true;
    }
    public void Options_Exit ()
    {
       
        Options_GBJ.GetComponent<Canvas>().enabled = false;
    }

    // Use this for initialization
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
