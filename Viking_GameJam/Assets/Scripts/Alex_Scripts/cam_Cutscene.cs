﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class cam_Cutscene : MonoBehaviour {

    //This goes in the Game Manager(GM_)

    public  GameObject MainCam;
    public GameObject CutSceneCam;
    Fade fade;
  //  public Destruc Des;

  

	// Use this for initialization
	void Start ()
    {
       


     //   MainCam = gameObject.transform.Find("Main Camera").gameObject;
      //  CutSceneCam = gameObject.transform.Find("CutScene_Cam").gameObject;
        fade = GetComponent<Fade>();

       // Do_It_Clone = Instantiate(DO_It , new Vector3(0.02f, -0.1f, -9.9f), Quaternion.identity) as GameObject;

        CutSceneCam.SetActive(false);
     
    }

    IEnumerator StartCutScene()
    {
        fade.BeginFade(1);
        

        yield return new WaitForSeconds(1);
        MainCam.SetActive(false);
        fade.BeginFade(-1);
        CutSceneCam.SetActive(true);

        yield return new WaitForSeconds(3f);
        fade.BeginFade(1);
        yield return new WaitForSeconds(1f);
        CutSceneCam.SetActive(false);
        fade.BeginFade(-1);
        MainCam.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        
    }
 

    // Update is called once per frame
    void Update ()
    {
        /*
        if(Des.AllGems == true)
        {
            StartCoroutine(StartCutScene());
            Des.AllGems = false;
           
        }
        */
        /*
        if (Input.GetKeyDown(KeyCode.F))
        {
            StartCoroutine(StartCutScene());
            fade.BeginFade(1);
          
        }

        if (Input.GetKeyDown(KeyCode.G))
        {

            fade.BeginFade(-1);
        }
        */


    }
}
