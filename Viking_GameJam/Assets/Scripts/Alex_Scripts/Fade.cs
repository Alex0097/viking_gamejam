﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour
{



    public Texture2D FadeOutTexture;
    public float FadeSpeed = 0.8f;

    public int drawDepth = -1000;
    private float Alpha = 1.0f;
    private int fadeDir = -1;



    void OnGUI()
    {
        Alpha += fadeDir * FadeSpeed * Time.deltaTime;
        Alpha = Mathf.Clamp01(Alpha);
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, Alpha);
        GUI.depth = drawDepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), FadeOutTexture);
    }
    public float BeginFade(int Dirrection)
    {
        fadeDir = Dirrection;
        return (FadeSpeed);
    }

    void OnLevelWasLoaded()
    {
        BeginFade(-1);
    }



}
