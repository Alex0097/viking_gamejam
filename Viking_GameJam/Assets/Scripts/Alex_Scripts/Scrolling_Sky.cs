﻿using UnityEngine;
using System.Collections;

public class Scrolling_Sky : MonoBehaviour {

    public float Speed;

	// Use this for initialization
	void Start ()
    {

        Speed = 0.01f;
	}
	
	// Update is called once per frame
	void Update ()
    {

        Vector2 Direc = new Vector2(Time.time * Speed,0);

        GetComponent<Renderer>().material.mainTextureOffset = Direc;
	
	}
}
