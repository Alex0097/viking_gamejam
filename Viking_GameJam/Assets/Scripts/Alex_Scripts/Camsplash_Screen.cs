﻿using UnityEngine;
using System.Collections;

public class Camsplash_Screen : MonoBehaviour {

    Fade fade;
    public GameObject Shia;
    AudioSource Do_IT;
    void Awake()
    {
        fade = GetComponent<Fade>();
        fade.BeginFade(1);
    }

	// Use this for initialization
	void Start ()
    {
        
        Do_IT = GetComponent<AudioSource>();
       
      StartCoroutine(Sell_out());
	
	}
    
    IEnumerator Sell_out()
    {
        yield return new WaitForSeconds(1);
        fade.BeginFade(-1);
        Do_IT.Play();
        yield return new WaitForSeconds(4f);
        fade.BeginFade(1);
       
        yield return new WaitForSeconds(1f);
        Shia.SetActive(false);
        fade.BeginFade(-1);
        
    }
    
    // Update is called once per frame
    void Update ()
    {
	
	}
}
