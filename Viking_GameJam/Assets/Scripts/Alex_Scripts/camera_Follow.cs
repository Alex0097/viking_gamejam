﻿using UnityEngine;
using System.Collections;

public class camera_Follow : MonoBehaviour {


    public Transform LookAt;

    private bool Smoot = true;
    
    public float smoothSpeed = 0.500f;
    private Vector3 OffSet = new Vector3(0, 0, -6.5f);
    

 

    // Use this for initialization
    void Start ()
    {
        smoothSpeed = 1f;
        OffSet = new Vector3(0, 0, -500f);

    }
	
	// Update is called once per frame
	void LateUpdate ()
    {
        Vector3 DesiredPostion = LookAt.transform.position + OffSet;

       
        if (Smoot)
        {
            transform.position = Vector3.Lerp(transform.position, DesiredPostion, smoothSpeed);
        }
        else
        {
            transform.position = DesiredPostion;

        }

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, 9, 49), Mathf.Clamp(transform.position.y, -62, -4), transform.position.z);
    }
}
