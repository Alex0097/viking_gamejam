﻿using UnityEngine;
using System.Collections;

public class M_Player : MonoBehaviour {
     
    Rigidbody2D RB;
    private float movSpeed;
    private float JumpForce;
    private bool Ground;

    public LayerMask floor;

    public Transform GroundPoint;
    public float radius;
    bool Pared;
    bool Suelo;

	// Use this for initialization
	void Start ()
    {
        radius = 0.1f;
        RB = GetComponent<Rigidbody2D>();
        JumpForce = 200;
        movSpeed = 5;
	}





    // Update is called once per frame
    void Update ()
    {

        Ground = Physics2D.OverlapCircle(GroundPoint.position, radius, floor);

       
            Vector2 Mov_Vec = new Vector2(Input.GetAxisRaw("Horizontal") * movSpeed, RB.velocity.y);

            RB.velocity = Mov_Vec;
       
        if(Input.GetAxisRaw("Horizontal") == -1)
        {
            transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
        }
        if (Input.GetAxisRaw("Horizontal") == 1)
        {
            transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
        }
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            RB.AddForce(new Vector2(0, JumpForce));
        }
        
        
        if (Input.GetButtonDown("A_Button")&& Ground)
        {
            RB.AddForce(new Vector2(0, JumpForce));
        }
        

    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(GroundPoint.position, radius);
    }
}
